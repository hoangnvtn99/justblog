﻿using JustBlog.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Entities.DataContext
{
    public static class DatabaseInitialization
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category()
                {
                    Id = 1,
                    CategoryName = "Category1",
                    UrlSlug = "Url1",
                    Description = "Description1"
                },
                new Category()
                {
                    Id = 2,
                    CategoryName = "Category2",
                    UrlSlug = "Url2",
                    Description = "Description2"
                },
                new Category()
                {
                    Id = 3,
                    CategoryName = "Category3",
                    UrlSlug = "Url3",
                    Description = "Description3"
                }
            );
            modelBuilder.Entity<Post>().HasData(
                 new Post()
                 {
                     Id = 1,
                     Title = "Title1",
                     ShortDescription = "ShortDescription1",
                     PostContent = "PostContent1",
                     UrlSlug = "UrlSlug1",
                     Published = "Published1",
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     CategoryId = 1
                 },
                 new Post()
                 {
                     Id = 2,
                     Title = "Title2",
                     ShortDescription = "ShortDescription2",
                     PostContent = "PostContent2",
                     UrlSlug = "UrlSlug2",
                     Published = "Published2",
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     CategoryId = 1
                 },
                 new Post()
                 {
                     Id = 3,
                     Title = "Title3",
                     ShortDescription = "ShortDescription3",
                     PostContent = "PostContent3",
                     UrlSlug = "UrlSlug3",
                     Published = "Published3",
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     CategoryId = 1
                 }
                );
            modelBuilder.Entity<Tag>().HasData(
                new Tag()
                {
                    Id = 1,
                    TagName = "TagName1",
                    UrlSlug = "UrlSlug1",
                    Description = "Description1",
                    Count = 1
                },
                new Tag()
                {
                    Id = 2,
                    TagName = "TagName2",
                    UrlSlug = "UrlSlug2",
                    Description = "Description2",
                    Count = 2
                },
                new Tag()
                {
                    Id = 3,
                    TagName = "TagName3",
                    UrlSlug = "UrlSlug3",
                    Description = "Description3",
                    Count = 3
                }
                );
            modelBuilder.Entity<PostTagMap>().HasData(
                new PostTagMap()
                {
                    PostId = 1,
                    TagId = 1,
                },
                new PostTagMap()
                {
                    PostId = 1,
                    TagId = 2,
                },
                new PostTagMap()
                {
                    PostId = 1,
                    TagId = 3,
                },
                new PostTagMap()
                {
                    PostId = 2,
                    TagId = 1,
                }, new PostTagMap()
                {
                    PostId = 2,
                    TagId = 3,
                },
                new PostTagMap()
                {
                    PostId = 3,
                    TagId = 1,
                }

                );
        }
    }
}