﻿using JustBlog.Entities.Config;
using JustBlog.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace JustBlog.Entities.DataContext
{
    public class JustBlogContext : DbContext
    {
        private readonly DbContextOptions _context;
        private readonly IConfiguration _configuration;
        private readonly IServiceCollection _services;

        public JustBlogContext()
        { }

        public JustBlogContext(DbContextOptions context, IConfiguration configuration, IServiceCollection services) : base(context)
        {
            _context = context;
            _configuration = configuration;
            _services = services;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=JustBlog;Integrated Security=True");
            //_services.AddDbContext<JustBlogContext>(options =>
            //{
            //    options.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"));
            //});
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PostConfig());

            modelBuilder.ApplyConfiguration(new PostTagMapConfig());

            modelBuilder.Entity<PostTagMap>().HasKey(t => new { t.PostId, t.TagId });

            modelBuilder.Seed();
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTagMap> PostTagMaps { get; set; }
    }
}