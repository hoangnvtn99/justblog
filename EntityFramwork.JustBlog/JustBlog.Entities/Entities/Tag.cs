﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JustBlog.Entities.Entities
{
    [Table("Tags", Schema = "Dbo")]
    public class Tag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Tag is required.", ErrorMessageResourceType = typeof(string))]
        [StringLength(50, ErrorMessage = "Short Description not greater than {0}")]
        public string TagName { get; set; } = string.Empty;

        [StringLength(255, ErrorMessage = "UrlSlug not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string UrlSlug { get; set; } = string.Empty;

        [Column("Description")]
        [StringLength(1000, ErrorMessage = "Description not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string Description { get; set; } = string.Empty;

        public int Count { get; set; }
        public virtual ICollection<PostTagMap>? PostTagMaps { get; set; }
    }
}