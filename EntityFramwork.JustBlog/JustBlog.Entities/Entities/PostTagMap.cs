﻿using System.ComponentModel.DataAnnotations.Schema;

namespace JustBlog.Entities.Entities
{
    [Table("PostTagMaps", Schema = "Dbo")]
    public class PostTagMap
    {
        [ForeignKey("PostId")]
        public int PostId { get; set; }

        public Post? Post { get; set; }

        [ForeignKey("TagId")]
        public int TagId { get; set; }

        public Tag? Tag { get; set; }
    }
}