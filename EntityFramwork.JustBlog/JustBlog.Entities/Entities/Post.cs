﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JustBlog.Entities.Entities
{
    [Table("Posts", Schema = "Dbo")]
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        [StringLength(50, ErrorMessage = "Title not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string Title { get; set; } = string.Empty;

        [Column("Short Description")]
        [StringLength(1000, ErrorMessage = "Short Description not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string ShortDescription { get; set; } = string.Empty;

        [Column("Post Content")]
        [StringLength(2000, ErrorMessage = "PostContent not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string PostContent { get; set; } = string.Empty;

        [StringLength(255, ErrorMessage = "UrlSlug not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string UrlSlug { get; set; } = string.Empty;

        [Required(ErrorMessage = "Published is required.", ErrorMessageResourceType = typeof(string))]
        public string Published { get; set; } = string.Empty;

        [Column("Posted On")]
        [Required(ErrorMessage = "PostedOn is required", ErrorMessageResourceType = typeof(DateTime))]
        public DateTime PostedOn { get; set; }

        public DateTime Modified { get; set; }

        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }

        public Category? Category { get; set; }
        public virtual ICollection<PostTagMap>? PostTagMaps { get; set; }
    }
}