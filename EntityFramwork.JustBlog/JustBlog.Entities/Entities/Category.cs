﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JustBlog.Entities.Entities
{
    [Table("Categories", Schema = "Dbo")]
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Category name is required.")]
        [StringLength(255, ErrorMessage = "CategoryName not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string CategoryName { get; set; } = string.Empty;

        [StringLength(255, ErrorMessage = "CategoryName not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string UrlSlug { get; set; } = string.Empty;

        [StringLength(1024, ErrorMessage = "CategoryName not greater than {0}", ErrorMessageResourceType = typeof(string))]
        public string Description { get; set; } = string.Empty;

        public virtual ICollection<Post>? Posts { get; set; }
    }
}