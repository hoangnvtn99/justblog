﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace JustBlog.Entities.Migrations
{
    public partial class v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Dbo");

            migrationBuilder.CreateTable(
                name: "Categories",
                schema: "Dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                schema: "Dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                schema: "Dbo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ShortDescription = table.Column<string>(name: "Short Description", type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    PostContent = table.Column<string>(name: "Post Content", type: "nvarchar(2000)", maxLength: 2000, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Published = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PostedOn = table.Column<DateTime>(name: "Posted On", type: "datetime2", nullable: false, defaultValue: new DateTime(2022, 10, 26, 16, 4, 40, 370, DateTimeKind.Local).AddTicks(7854)),
                    Modified = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "Dbo",
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostTagMap",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false),
                    TagId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTagMap", x => new { x.PostId, x.TagId });
                    table.ForeignKey(
                        name: "FK_PostTagMap_Posts_PostId",
                        column: x => x.PostId,
                        principalSchema: "Dbo",
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostTagMap_Tags_TagId",
                        column: x => x.TagId,
                        principalSchema: "Dbo",
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "Dbo",
                table: "Categories",
                columns: new[] { "Id", "CategoryName", "Description", "UrlSlug" },
                values: new object[,]
                {
                    { 1, "Category1", "Description1", "Url1" },
                    { 2, "Category2", "Description2", "Url2" },
                    { 3, "Category3", "Description3", "Url3" }
                });

            migrationBuilder.InsertData(
                schema: "Dbo",
                table: "Tags",
                columns: new[] { "Id", "Count", "Description", "TagName", "UrlSlug" },
                values: new object[,]
                {
                    { 1, 1, "Description1", "TagName1", "UrlSlug1" },
                    { 2, 2, "Description2", "TagName2", "UrlSlug2" },
                    { 3, 3, "Description3", "TagName3", "UrlSlug3" }
                });

            migrationBuilder.InsertData(
                schema: "Dbo",
                table: "Posts",
                columns: new[] { "Id", "CategoryId", "Modified", "Post Content", "Posted On", "Published", "Short Description", "Title", "UrlSlug" },
                values: new object[] { 1, 1, new DateTime(2022, 10, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "PostContent1", new DateTime(2022, 10, 26, 16, 4, 40, 371, DateTimeKind.Local).AddTicks(1843), "Published1", "ShortDescription1", "Title1", "UrlSlug1" });

            migrationBuilder.InsertData(
                schema: "Dbo",
                table: "Posts",
                columns: new[] { "Id", "CategoryId", "Modified", "Post Content", "Posted On", "Published", "Short Description", "Title", "UrlSlug" },
                values: new object[] { 2, 1, new DateTime(2022, 10, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "PostContent2", new DateTime(2022, 10, 26, 16, 4, 40, 371, DateTimeKind.Local).AddTicks(1913), "Published2", "ShortDescription2", "Title2", "UrlSlug2" });

            migrationBuilder.InsertData(
                schema: "Dbo",
                table: "Posts",
                columns: new[] { "Id", "CategoryId", "Modified", "Post Content", "Posted On", "Published", "Short Description", "Title", "UrlSlug" },
                values: new object[] { 3, 1, new DateTime(2022, 10, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "PostContent3", new DateTime(2022, 10, 26, 16, 4, 40, 371, DateTimeKind.Local).AddTicks(1919), "Published3", "ShortDescription3", "Title3", "UrlSlug3" });

            migrationBuilder.InsertData(
                table: "PostTagMap",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 2, 1 },
                    { 2, 3 },
                    { 3, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Posts_CategoryId",
                schema: "Dbo",
                table: "Posts",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PostTagMap_TagId",
                table: "PostTagMap",
                column: "TagId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PostTagMap");

            migrationBuilder.DropTable(
                name: "Posts",
                schema: "Dbo");

            migrationBuilder.DropTable(
                name: "Tags",
                schema: "Dbo");

            migrationBuilder.DropTable(
                name: "Categories",
                schema: "Dbo");
        }
    }
}
